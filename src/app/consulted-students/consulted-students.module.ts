import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsultedStudentsTableComponent } from './components/consulted-students-table/consulted-students-table.component';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import { TablesModule } from '@universis/ngx-tables';
import { ConsultedStudentsRoutingModule } from './consulted-students-routing.module';
import { ConsultedStudentsHomeComponent } from './components/consulted-students-home/consulted-students-home.component';
// tslint:disable-next-line:max-line-length
import { ConsultedStudentsDefaultTableConfigurationResolver, ConsultedStudentsTableConfigurationResolver, ConsultedStudentsTableSearchResolver } from './components/consulted-students-table/consulted-students-table-config.resolver';
import { ConsultedStudentsSharedModule } from './consulted-students.shared';

import { SendMessageActionComponent } from './components/send-message-action/send-message-action.component';
import { MessagesSharedModule } from '../messages/messages.shared';
import { SharedModule } from '@universis/common';
import {RouterModalModule} from '@universis/common/routing';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        ConsultedStudentsSharedModule,
        TablesModule,
        SharedModule,
        ConsultedStudentsRoutingModule,
        RouterModalModule
    ],
  declarations: [
    ConsultedStudentsHomeComponent,
    ConsultedStudentsTableComponent
  ],
  entryComponents: [
    SendMessageActionComponent
  ],
  providers: [
    ConsultedStudentsTableConfigurationResolver,
    ConsultedStudentsDefaultTableConfigurationResolver,
    ConsultedStudentsTableSearchResolver
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ConsultedStudentsModule {
  constructor(private _translateService: TranslateService) {
    environment.languages.forEach((culture) => {
      import(`./i18n/consulted-students.${culture}.json`).then((translations) => {
        this._translateService.setTranslation(culture, translations, true);
      });
    });
  }
}

